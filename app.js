﻿var express = require("express");
var bodyParser = require("body-parser");

var app = express();

app.use("/estaticos", express.static('public'));
app.use("/estaticos2", express.static('assets'));
app.use(bodyParser.json()); //para peticion con el encabezado application/json
app.use(bodyParser.urlencoded({extended:true}));

app.set("view engine", "jade");

app.get("/", function (peticion, respuesta) {
    respuesta.render("index");
});

app.get("/login", function (peticion, respuesta) {
    
    respuesta.render("login");
});

app.post("/usuarios", function (peticion, respuesta) {
    console.log("Usuario: "+peticion.body.email);
    console.log("Contraseña: " + peticion.body.contrasenia);
    respuesta.send("Recibimos tus datos");
});

app.listen(3000);
